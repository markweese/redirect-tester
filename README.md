# redirect-tester

Web application which leverages a home cooked flask API to test redirected URLs and outputs to a dashboard for user review.

## Local Testing

1. CD into the project directory and [initialize the venv](https://docs.python.org/3/library/venv.html)
2. Navigate to the /app/api.py file and on line 3 swap `from .redirect_functions import test_redirect, parse_urls` with `from redirect_functions import test_redirect, parse_urls`
3. CD from the root into /app and run `python3 api.py`
4. Site will be live at `http://127.0.0.1:5000`